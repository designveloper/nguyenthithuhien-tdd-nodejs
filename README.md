*** TDD - NodeJS ***

## 1. Understanding Test- Driven Development
### A note on test-first Development
- Test-Driven Development
- Behavior-Driven Development
### What is testing and test first
- Traditional Testing:
  + Plan and write code
  + Human tests full app
  + Cons: requires some sort of human element and the process might be slow, also miss some things.
- Programmatic testing,
  + Plan and write code:
  + Program checks individual sections of code.
    + Make sure another program is working normally
    + When we test very small, small enough to check just one feature or some sort of isolated functionality of the app. These are called unit tests.
    + When these tests are much bigger, and they test multiple features working together, or one system communicating with another or interacting with it, these test are called integration tests.
- Tests are small programs that test the functionality of our main program.
- Test first:
  + Write test
  + Fail test
  + Code to pass test

## 2. Testing your data Layer
### Introducing the Mocha testing framework
- 2 frameworks help us write tests efficiently: Mocha and Should.js
- Mocha: runs the tests and display the results.
  + Link: https://mochajs.org/
- Should.js : allow us to assert our tests in a very friendly and readable way.
  + Link: https://shouldjs.github.io/

- Use Mocha:
  + Install Mocha globally: npm install mocha -g
  + Mocha requires that we have a `test folder`, it will look for any files that have spec.js in the file titles and it will try to run all the tests it finds within those files.

### Creating your first test
- `Describe` the context of all the tests in these files or specification of this group of features we will be testing.
  + describe('param1', param2)
    + param1: title these test as 'user data'
    + param2: callback function that contains all of our tests.
- To begin with, we'll be writing tests for objects that don't yet exist. After that, we create those objects.
- `it` : defining our test
  + it(param1, param2)
    + param1: title of test
    + param2: generator function
- `should` package  

### Making sure you fail first
- Mocha does not know how to handle generator functions. To Mocha, it seems as if this is a regular function and that there is no code inside.
- So, we need install `co-mocha` and `require` it

### Getting your test to pass
- npm install should
- `co-fs`: give us file system access with generators.
- before: allows us to run some code before all our test.
  + before(callback generators function)

## 3. Testing your web Layer
### Introducing the SuperTest framework
- Use `SuperTest package` to test web API or web Layer
- Because I use generators, I need to use the co version:  co-supertest
- SuperTest needs an` instance of Koa` passed into it.  
  + Install `koa` and require it.
  + Install `koa-router` 


### Refactoring your codee
### Trying the final product

