require('co-mocha');
var should = require('should');
var data = require('../user-data.js');
var fs = require('co-fs');
var api = require('../user-web.js' );
var request = require('co-supertest').agent(api.listen());


before(function *(){
    yield fs.writeFile('./users.json', '[]');
});

describe('user data', function() {
  it('should have +1 user count after adding', function* (){
    // Get count users
    var users = yield data.users.get();

    //Add new user
    yield data.users.add({name: 'Hien'});

    //Get new count users
    var newUsers = yield data.users.get();

    newUsers.length.should.equal(users.length + 1);
  });
});

describe('user web', function() {
    it('should have +1 user count after adding', function* () {
        // Get count users
        var users = (yield request.get('/user').expect(200).end()).body;

        //Add new user
        yield data.users.add({ name: 'Hien' });

        //Get new count users
        var newUsers = (yield request.get('/user').expect(200).end()).body;

        newUsers.length.should.equal(users.length + 1);
    });
});
